
<?php defined('BASEPATH') OR exit('No direct script access allowed');


  class Aws_Jaipur extends CI_Controller  {
  
public function __construct()
    {
       $data=array();
		parent::__construct();
        
        $this->table_name='registrations';
         $this->load->helper('grocery_crud_helper');
        $this->load->model('Login_auth_db');
        $this->load->model('registrationModel');
        $this->load->library('form_validation');
        $this->load->library('Grocery_CRUD');
        
        $this->className=$this->router->fetch_class();
        
    }
    public function index()
    {
        
        $data['eventDetails']=getEvent('id',$this->router->fetch_class());
      
        if($this->input->post('username'))
		{

            $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == true) {

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if ($this->Login_auth_db->login($username,$password)) {
				$user_data = $this->Login_auth_db->get_user_data($username);

                if($user_data['level']=='event')
                {
                    $checkuserforthisevent=checkuserRegisteredForthisEvent($this->className,$user_data['event']);
                    if(count($checkuserforthisevent>0))
                    {
                       $this->Login_auth_db->setSession($user_data); 
                    }else
                    {
                      $data['error'] = 'You are not a valid user for this event.';  
                    }
                    
                }else
                {
                     $this->Login_auth_db->setSession($user_data); 
                }
	
				redirect('admin/'.$this->className.'/home');
				
			} else {

				 $data['error'] = 'Wrong username or password.';
            }

            }           
        
        }
        
       $this->load->view('admin/login',$data);  
    }
      
    public function AutoLogin($userID)
    {
       redirect('admin/'.$this->className.'/home');
    }

    public function home()
    {
        $data['content'] = getEvent('id',$this->router->fetch_class())['event_name'] .'dashboard';
		$data['count']['TotalDelegates']=1;
		$data['count']['TotalOtherAttendee']=2;
        $this->load->view('event_admin/event_admin_dashboard',$data);
    }

    public function test() {
   
      $query =  $this->db->query("SELECT created_at as y_date, DAYNAME(created_at) as day_name, COUNT(id) as count  FROM users WHERE (MONTH(created_at) = '11' OR MONTH(created_at) = '12') AND YEAR(created_at) = '2019' GROUP BY DAYNAME(created_at) ORDER BY (y_date) ASC"); 
 
      $record = $query->result();
      $data = [];
 $subs=array('type'=>'Oil','percent'=>'15');
 $i=0;
      foreach($record as $row) {
            $data[$i]['type'] = $row->day_name;
            $data[$i]['percent'] = (int) $row->count;
            $data[$i]['color'] ="#ff9e01";
            $data[$i]['subs'][] =$subs;
$i++;
      }
      echo(json_encode($data));
     
    }
    
     public function onsiteRegistrations()
    {
         $data['result']=getOnsitelist($this->className);
         $this->load->view('event_admin/event_onsite_links',$data);
         
    }

    
    //registrations/pre/day1/morning
    //registrations/onsite/day1/morning
    public function registrations($type,$day,$id='',$session='')
    {
     
        
          if($type==='pre')
            {
                $typeid=0;
            }
            if($type==='onsite')
            {
                $typeid=1;
            }
        
            $eventID=getEvent('id',$this->className)['idevents']; 
        
try{
     $crud = _getGroceryCrudEnterprise();
    $crud->setTable($this->table_name);
    $crud->setSubject('Registrations',ucwords($type).'Registrations '.$day.' '.$session);
   $crud->columns(['company_name','full_name','mob_number','uniq_code','user_type','registration_status']);
    $crud->fieldType('registration_type','dropdown',array('0' => 'Pre', '1' => 'Onsite'));
    $crud->fieldType('registration_status','dropdown',array('0' => 'Pending', '1' => 'Confirmed'));
    			//$crud->where('day_id',$day);
$crud->unsetAdd();

    $crud->where(['day_id = ?' => $day,'registration_type = ?' => $typeid,'event_id = ?' => $eventID]);
        if(!empty($session))
    {
        $crud->where(['shift_id = ?' => $session]);

    }

       
    $output = $crud->render();
    //$output = array_merge($data,(array)$output);
    // $data['title']=ucwords($type).'Registrations '.$day.' '.$session;

   // $output->data = $data;
    _example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
        
         
    }
      
      
      public function attendee($type,$day,$id='',$session='')
    {
     
        
          if($type==='pre')
            {
                $typeid=0;
            }
            if($type==='onsite')
            {
                $typeid=1;
            }
        
            $eventID=getEvent('id',$this->className)['idevents']; 
        
try{
     $crud = _getGroceryCrudEnterprise();
    $crud->setTable($this->table_name);
    $crud->setSubject('Registrations',ucwords($type).'Registrations '.$day.' '.$session.' Attendees');
   $crud->columns(['company_name','full_name','mob_number','uniq_code','user_type','registration_status']);
    $crud->fieldType('registration_type','dropdown',array('0' => 'Pre', '1' => 'Onsite'));
    $crud->fieldType('registration_status','dropdown',array('0' => 'Pending', '1' => 'Confirmed'));
    			//$crud->where('day_id',$day);
$crud->unsetAdd();

    $crud->where(['day_id = ?' => $day,'registration_type = ?' => $typeid,'event_id = ?' => $eventID,'registration_status = ?' => 1]);
        if(!empty($session))
    {
        $crud->where(['shift_id = ?' => $session]);

    }

       
    $output = $crud->render();
    // $data['title']=ucwords($type).'Registrations '.$day.' '.$session.' Attendees';

    // $output->data = $data;
    _example_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
        
         
    }
      
      
 
    //     public function csv_import()
    // {
    //     $data['result']=checkPermission($this->className);
    //     $this->load->view('admin/event_csv_import',$data);
    // }
  
  
  }?>