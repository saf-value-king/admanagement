<?php if (!defined('BASEPATH')) {
	exit('no direct access script allowed');
}

class Dashboard extends CI_Controller {

	public function __construct() 
	{

		parent::__construct();
		
		$this->load->helper('grocery_crud_helper');

        $this->load->library('Grocery_CRUD');
        $this->load->model('Login_auth_db');
		
	}

	public function index() 
	{
		
		$this->load->view('admin/dashboard');
		
	}
		
		  public function client()
    {
                
		try
		{
			$crud = _getGroceryCrudEnterprise();
			$crud->setTable("client_details");
			$crud->columns(['customer_first_name','customer_acct_type','customer_email_address','company_name','company_address','phone_number']);
			$crud->fieldType('customer_acct_type','dropdown',array('individual' => 'Individual', 'company' => 'Company'));
			$crud->fieldType('status','dropdown',array('0' => 'Inactive', '1' => 'Active'));
			
			$crud->callbackAfterInsert(function ($stateParameters) {
			$this->db->where('cusID', $stateParameters->insertId);
			$customer = $this->db->get('client_details')->row();

			if (!empty($customer)) {
			$mailDataArray=array('toaddress'=>$customer->customer_email_address,
								'subject'=>'Test Subject',
								'message'=>'Test Message');
								
								$insert=array('name'=>$customer->customer_first_name.' '.$customer->customer_last_name,
												'client_id'=>$stateParameters->insertId,
												'username'=>$customer->customer_email_address,
												'password'=>'123',
												'level'=>'client',
												'status'=>'Active');
			$this->db->insert('admin_users',$insert);									
			
			//emailSend($mailDataArray);
			}

			return $stateParameters;
			});

			$output = $crud->render();
			$data['title']="Client Management";
			$output->data = $data;
			_example_output($output);

		}catch(Exception $e)
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}      
         
    }
	
	public function emailSend($mailDataArray)
	{
		   $this->email->clear();

        $this->email->to($mailDataArray['toaddress']);
        $this->email->from('your@example.com');
        $this->email->subject($mailDataArray['subject']);
        $this->email->message($mailDataArray['message']);
        $this->email->send();
	}
	
	public function change_pass()
	{
		if($this->input->post('change_pass'))
		{
			$old_pass=$this->input->post('old_pass');
			$new_pass=$this->input->post('new_pass');
			$confirm_pass=$this->input->post('confirm_pass');
			$session_id=$this->session->userdata('id');
			$que=$this->db->query("select * from admin_users where id='$session_id'");
			$row=$que->row();
			if((!strcmp($old_pass, $pass))&& (!strcmp($new_pass, $confirm_pass))){
				$this->Login_auth_db->change_pass($session_id,$new_pass);
				echo "Password changed successfully !";
				}
			    else{
					echo "Invalid";
				}
		}
		$this->load->view('change_pass');	
	}
		
		}
		
		?>