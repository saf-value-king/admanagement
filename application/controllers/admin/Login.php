<?php if (!defined('BASEPATH')) {
	exit('no direct access script allowed');
}

class Login extends CI_Controller {

	public function __construct() {

		parent::__construct();
		// load form helper and validation library
		$this->load->library('form_validation');

		$this->load->model('Login_auth_db');

		/*if ( ! $this->session->userdata('logged_in'))
			        {
			            redirect('admin/login');
		*/

	}

	public function index() {
		$data = array();
		if ($this->input->post('username')) {
            // set validation rules
			$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if ($this->form_validation->run() == false) {
                // validation not ok, send validation errors to the view
				$this->load->view('admin/login');
			} else {
                // set variables from the form
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				if ($this->Login_auth_db->login($username, $password)) {
					$user_data = $this->Login_auth_db->get_user_data($username);
					// if ($user_data['level'] == 'Super') {
					$this->Login_auth_db->setSession($user_data);
					if($user_data['level']!='client')
					{
							redirect('admin/dashboard');
					}else
					{
							redirect('advertisers/dashboard');
					}
					// 	redirect('admin/login/home');
					// } else {
					// 	$data['error'] = 'You dont have access';
					// }
				

				} else {
                    // login failed
					$data['error'] = 'Wrong username or password.';
				}
			}
		}
		$this->load->view('admin/login', $data);

	}

	public function Logout() {
		$this->session->sess_destroy();
		redirect('admin/login');
	}

}