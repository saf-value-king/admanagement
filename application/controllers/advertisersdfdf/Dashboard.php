<?php if (!defined('BASEPATH')) {
	exit('no direct access script allowed');
}

class Dashboard extends CI_Controller {

	public function __construct() 
	{

		parent::__construct();
		
		$this->load->helper('grocery_crud_helper');

        $this->load->library('Grocery_CRUD');
        
		
	}

	public function index() 
	{
		
		$this->load->view('advertisers/dashboard');
	}
		
		  public function client()
    {
                
		try
		{
			$crud = _getGroceryCrudEnterprise();
			$crud->setTable("client_details");
			$crud->columns(['customer_first_name','customer_acct_type','customer_email_address','company_name','company_address','phone_number']);
			$crud->fieldType('customer_acct_type','dropdown',array('individual' => 'Individual', 'company' => 'Company'));
			$crud->fieldType('status','dropdown',array('0' => 'Inactive', '1' => 'Active'));
			$output = $crud->render();
			$data['title']="Client Management";
			$output->data = $data;
			_example_output($output);

		}catch(Exception $e)
		{
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}      
         
    }
		
		}
		
		?>