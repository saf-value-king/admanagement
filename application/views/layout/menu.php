
 <header>
            <div class="head_container">
                <div class="logo">
                    <a href="<?=base_url();?>" class="full_logo"><img src="<?=base_url();?>assets/site/images//iff_logo.svg" id="main_logo" alt="IFF logo" /></a>
                    <a href="<?=base_url();?>" class="fixed_logo"><img src="<?=base_url();?>assets/site/images//ifftxt_fixedlogo.png" alt="IFF logo" /></a>
                </div>
                <div class="nav_info">
                    <nav class="navbar navbar-default navbar-expand-md">
                        <button class="navbar-toggler custom-toggle-color" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end custom-toggle-bg" id="collapsibleNavbar">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link nav-color" href="#">Conference <span></span></a>
                                    <ul class="sub_menu">
                                        <li><a href="<?=base_url();?>tracks-and-sessions">Tracks & Sessions</a></li>
                                        <li><a href="<?=base_url();?>agenda">Agenda</a></li>
                                        <li><a href="<?=base_url();?>speakers">Speakers</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-color" href="#">Highlights <span></span></a>
                                    <ul class="sub_menu">
                                        <li><a href="<?=base_url();?>awards">Awards</a></li>
                                        <li><a href="<?=base_url();?>leopard-leap">Leopard Leap</a></li>
                                        <li><a href="<?=base_url();?>cxo-dinner">CxO Dinner</a></li>
                                        <li><a href="<?=base_url();?>celebrity-track">Celebrity Track </a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-color" href="#">Exhibit <span></span></a>
                                    <ul class="sub_menu">
                                        
                                        <li><a href="<?=base_url();?>exhibition-packages">Exhibition Packages</a></li>
                                        <li><a href="#">Exhibitor List</a></li>
                                        <li><a href="#">Floor Plan  </a></li>
                                    </ul>
                                </li>
                                
                                
                                <li class="nav-item">
									<a class="nav-link nav-color" href="#">Zones <span></span></a>
                                    <ul class="sub_menu">
                                       
                                       
                                        <li><a href="<?=base_url();?>zones/investment">Investment</a></li>
                                        <li><a href="#">Interaction</a></li>
                                       
                                        <li><a href="#">Learning</a></li>
                                        <li><a href="#">Experience</a></li>
                                      
                                    </ul>
                                </li>
                                
                                
                                <li class="nav-item">
                                    <a class="nav-link nav-color" href="<?=base_url();?>tickets">Tickets <span></span></a>
                                    <ul class="sub_menu">
                                        <li><a href="<?=base_url();?>tickets">Buy Tickets</a></li>
                                        <!-- <li><a href="#">Ticket Policy</a></li> -->
                                    </ul>
                                </li>
                                <li class="nav-item current">
                                    <a class="nav-link nav-color" href="<?=base_url();?>contact">Contact</a>
                                </li>
                              
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>